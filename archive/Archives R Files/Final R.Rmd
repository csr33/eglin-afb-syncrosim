---
title: "Syncrosim Demo"
author: "Celine Robinson"
date: "10/13/2020"
output: html_document
---

# SyncroSim State and Transition Model
In this next section we will run the STSM state and transition model. This will provide simulations of the Longleaf pine state class through time for the 'No Management' and 'Current Management' scenarios 

## R Setup
Install and Load Packages
```{r, warning=FALSE, results='hide',message=FALSE}
#install.packages("rsyncrosim")
#install.packages("tidyverse")
#install.packages("blscrapeR")
#install.packages("jsonlite", type = "source")
#install.packages("devtools")
library(jsonlite)
library(devtools)
#install_github('mikeasilva/blsAPI')
library(blsAPI)
library(dplyr)
#library(blscrapeR)
library(rsyncrosim)
library(raster)
library(sp)
library(tidyverse)
```

Print Out Packages
```{r}
sessionInfo()
```

#Set random seed
```{r}
set.seed(1)
```


## Set-up SyncroSim
Start a SyncroSim Session
```{r}
# Set the name of the folder into which you installed SyncroSim  (i.e. this folder should contain the file SyncroSim.Console.exe)

programFolder = "C:/Program Files/SyncroSim"
mySession = session(programFolder)
```
Create SyncroSim Library
```{r, results = "hide"}
# Create a new ST-Sim library in the current working directory (or load it if it already exists)

setwd("C:/Users/13022/Box/SERDP ES Project/Model and Results (2019)/Eglin Case Study/SyncroSim Model")
myLibrary = ssimLibrary(name="Eglin Nonspatial Library",session=mySession)

# Note that it can be helful to have this library open in the SyncroSim user interface at the same time you are modifying the library in R.
# This way, by periodically invoking the 'File-Refresh All Libraries' menu in SyncroSim, you can then see the latest changes you made in R

# Display internal names of all the library's datasheets - corresponds to the the 'File-Library Properties' menu in SyncroSim
librarySheetNames = datasheet(myLibrary, summary=T)
View(librarySheetNames)
# Backup the library before making changes
backup(myLibrary)
```
Create SyncroSim Project
```{r}
# Create a new SyncroSim project (or open it if it already exists)
myProject = project(myLibrary, project="longleaf")

# Display internal names of all the project's datasheets
View((projectSheetNames=datasheet(myProject, summary=T, optional=F)))
```


## Edit the Project Definitions
This corresponds to the 'Project-Properties' menu in SyncroSim.
Note: An STSim_ prefix for a datasheet name is no longer required. Warnings may pop up after running the following code. 

Terminology
```{r, results = "hide"}
(sheetData = datasheet(myProject, "STSim_Terminology"))
str(sheetData)
sheetData$AmountLabel[1] = "Hectares"
sheetData$AmountUnits[1] = "Hectares"
sheetData$StateLabelX[1] = "Cover Type"
sheetData$StateLabelY[1] = "Structural Stage"
sheetData$PrimaryStratumLabel[1] = "Vegetation Type"
sheetData$SecondaryStratumLabel[1] = "Planning Zone"
sheetData$TertiaryStratumLabel[1] = "Tertiary Stratum"
sheetData$TimestepUnits[1] ="Timestep"
saveDatasheet(myProject, sheetData, "STSim_Terminology")
```
Stratum (Strata)
```{r, results='hide',message=FALSE}
Vegetation_Type <- read_csv("./tables/Vegetation Type.csv", 
    col_types = cols(Color = col_skip(), 
        Legend = col_skip()))
saveDatasheet(myProject, Vegetation_Type, "STSim_Stratum", append =F, force=T)
View(datasheet(myProject, "STSim_Stratum", optional=T))   # Returns entire dataframe, including optional columns
```
States
```{r,  results = "hide"}
# First State Class Label (Cover Type)
Cover_Type <- read_csv("./tables/Cover Type.csv")
saveDatasheet(myProject, data.frame(Cover_Type), "STSim_StateLabelX",append = F, force=T)

# Second State Label (Structural Stage)
Structural_Stage <- read_csv("./tables/Structural Stage.csv")
saveDatasheet(myProject, data.frame(Structural_Stage), "STSim_StateLabelY",append = F, force=T)

# State Class
State_Class <- read_csv("./tables/State Class.csv", 
    col_types = cols(Color = col_skip(), 
        Legend = col_skip()))
saveDatasheet(myProject, data.frame(State_Class), "STSim_StateClass",append = F, force=T)
```
Define Transitions
```{r,  results = "hide"}
# Transition Types
Transition_Type <- read_csv("./tables/Transition Type.csv", 
    col_types = cols(Color = col_skip(), 
        Legend = col_skip()))
saveDatasheet(myProject, data.frame(Transition_Type), "STSim_TransitionType",append = F, force = T)

# Transition Groups - make the Groups identical to the Types
Transition_Group <- read_csv("./tables/Transition Group.csv", 
    col_types = cols(IsAuto = col_skip()))
saveDatasheet(myProject, data.frame(Transition_Group), "STSim_TransitionGroup",append = F, force = T)
```
Advanced
```{r ,   echo=FALSE}
# Transition Types by Groups  - assign each Type to its Group
Transition_Types_by_Group <- read_csv("./tables/Transition Types by Group.csv",
  col_types = cols(IsAuto = col_skip()))
saveDatasheet(myProject, data.frame(Transition_Types_by_Group), "STSim_TransitionTypeGroup",append = F, force = T)

# Transition Simulation Groups  
Transition_Simulation_Groups <- read_csv("./tables/Transition Simulation Groups.csv")
saveDatasheet(myProject, data.frame(Transition_Simulation_Groups), "STSim_TransitionSimulationGroup",append = F, force = T)

# Transition Multiplier
Transition_Multiplier_Type <- read_csv("./tables/Transition Multiplier Type.csv")
saveDatasheet(myProject, Transition_Multiplier_Type, "STSim_TransitionMultiplierType",append = F, force = T)
```


## Create Scenarios
###No Management Scenario
Define 
```{r}
# Create/open a SyncroSim "Eglin No Management" scenario
NMScenario = scenario(myProject, "No Management Scenario")

# Display the internal names of all the scenario data sheets - these correspond to the 'Scenario-Properties' menu in SyncroSim (when a Scenario is selected)
View((scenarioSheetNames=subset(datasheet(NMScenario, summary=T), scope=="scenario")))

# Edit the scenario data sheets:

# Run Control - Note that we will set this as a spatial run
sheetName = "STSim_RunControl"
sheetData = data.frame(MaximumIteration=10000, MinimumTimestep=0, MaximumTimestep=60, isSpatial=F)
saveDatasheet(NMScenario, sheetData, sheetName)

# States
sheetName = "STSim_DeterministicTransition"
BaseScenario_States <- read_csv("./tables/Scenario.BaseScenario.States.csv", 
    col_types = cols(Iteration = col_skip(), 
        Timestep = col_skip()))
sheetData = data.frame(BaseScenario_States)
saveDatasheet(NMScenario, sheetData, sheetName)
```
Probabilistic Transitions
```{r}
sheetName = "STSim_Transition"
BaseScenario_TransitionPathways <- read_csv("./tables/Scenario.BaseScenario.TransitionPathways.csv", 
    col_types = cols(Iteration = col_skip(), 
        SecondaryStratumID = col_skip(), 
        TertiaryStratumID = col_skip(), Timestep = col_skip()))
sheetData= data.frame(BaseScenario_TransitionPathways)
saveDatasheet(NMScenario, sheetData, sheetName)
```
Initial Conditions
There are two options for setting initial conditions, we are setting nonspatial initial conditions.
```{r}
sheetName = "STSim_InitialConditionsNonSpatial"
sheetData = data.frame(TotalAmount=148637.000, CalcFromDist=F)
#Number of Cells?
saveDatasheet(NMScenario, sheetData, sheetName)
datasheet(NMScenario, sheetName)

sheetName = "STSim_InitialConditionsNonSpatialDistribution"
Scenario.NoManagment_InitialCondDistribution <- read_csv("./tables/Scenario.NoManagment.InitialCondDistribution.csv", 
    col_types = cols(AgeMax = col_skip(), 
        AgeMin = col_skip(), Iteration = col_skip(), 
        SecondaryStratumID = col_skip(), 
        TertiaryStratumID = col_skip()))
sheetData = Scenario.NoManagment_InitialCondDistribution
#View(Scenario.NoManagment_InitialCondDistribution)
saveDatasheet(NMScenario, sheetData, sheetName)
datasheet(NMScenario, sheetName)
```
Transition targets 
```{r}
#set harvest to 0 for this scenario
Scenario.NoManagement_TransitionTargets <- read_csv("./tables/Scenario.NoManagement.TransitionTargets.csv", 
    col_types = cols(DistributionFrequencyID = col_skip(), 
        DistributionMax = col_skip(), DistributionMin = col_skip(), 
        DistributionSD = col_skip(), DistributionType = col_skip(), 
        Iteration = col_skip(), SecondaryStratumID = col_skip(), 
        StratumID = col_skip(), TertiaryStratumID = col_skip(), 
        Timestep = col_skip()))

saveDatasheet(NMScenario, Scenario.NoManagement_TransitionTargets, "STSim_TransitionTarget")
```
Output options
```{r}
datasheet(NMScenario, "STSim_OutputOptions")
sheetData = data.frame(SummaryOutputSC=T, SummaryOutputSCTimesteps=1,
                       SummaryOutputTR=T, SummaryOutputTRTimesteps=1,
                       RasterOutputSC=T, RasterOutputSCTimesteps=1,
                       RasterOutputTR=T, RasterOutputTRTimesteps=1,
                       RasterOutputAge=T, RasterOutputAgeTimesteps=1)
saveDatasheet(NMScenario, sheetData, "STSim_OutputOptions")
```


### Current Management Scenario   
Define
```{r}
# Create/open a SyncroSim "Eglin Current Management" scenario
CMScenario = scenario(myProject, "Current Management Scenario")

# Display the internal names of all the scenario data sheets - these correspond to the 'Scenario-Properties' menu in SyncroSim (when a Scenario is selected)
View((scenarioSheetNames=subset(datasheet(CMScenario, summary=T), scope=="scenario")))

# Edit the scenario data sheets:

# Run Control - Note that we will set this as a non spatial run
sheetName = "STSim_RunControl"
sheetData = data.frame(MaximumIteration=10000, MinimumTimestep=0, MaximumTimestep=20, isSpatial=F)
saveDatasheet(CMScenario, sheetData, sheetName)

# States
sheetName = "STSim_DeterministicTransition"
BaseScenario_States <- read_csv("./tables/Scenario.BaseScenario.States.csv", 
    col_types = cols(Iteration = col_skip(), 
        Timestep = col_skip()))
sheetData = data.frame(BaseScenario_States)
saveDatasheet(CMScenario, sheetData, sheetName)
```
Probabilistic Transition
```{r}
# Probabilistic Transitions
sheetName = "STSim_Transition"
BaseScenario_TransitionPathways <- read_csv("./tables/Scenario.BaseScenario.TransitionPathways.csv", 
    col_types = cols(Iteration = col_skip(), 
        SecondaryStratumID = col_skip(), 
        TertiaryStratumID = col_skip(), Timestep = col_skip()))
sheetData= data.frame(BaseScenario_TransitionPathways)
saveDatasheet(CMScenario, sheetData, sheetName)
```
Initial Conditions
```{r}
# Initial Conditions set to non-spatial
sheetName = "STSim_InitialConditionsNonSpatial"
sheetData = data.frame(TotalAmount=148637.000, CalcFromDist=F)
#Number of Cells?
saveDatasheet(CMScenario, sheetData, sheetName)
datasheet(CMScenario, sheetName)

sheetName = "STSim_InitialConditionsNonSpatialDistribution"
ScenarioCurrentConditions_InitialCondDistribution <- read_csv("./tables/Scenario.CurrentConditions.InitialCondDistribution.csv", 
    col_types = cols(AgeMax = col_skip(), 
        AgeMin = col_skip(), Iteration = col_skip(), 
        SecondaryStratumID = col_skip(), 
        TertiaryStratumID = col_skip()))
sheetData = ScenarioCurrentConditions_InitialCondDistribution
#View(ScenarioCurrentConditions_InitialCondDistribution)
saveDatasheet(CMScenario, sheetData, sheetName)
datasheet(CMScenario, sheetName)
```
Transition Targets
```{r}
# Transition targets - set harvest to 0 for this scenario
Scenario_CurrentConditions_TransitionTargets <- read_csv("./tables/Scenario.CurrentConditions.TransitionTargets.csv", 
    col_types = cols(DistributionFrequencyID = col_skip(), 
        DistributionMax = col_skip(), DistributionMin = col_skip(), 
        DistributionSD = col_skip(), DistributionType = col_skip(), 
        Iteration = col_skip(), SecondaryStratumID = col_skip(), 
        StratumID = col_skip(), TertiaryStratumID = col_skip(),  
        Timestep = col_skip()))

saveDatasheet(CMScenario, Scenario_CurrentConditions_TransitionTargets, "STSim_TransitionTarget")
```
Output Options
```{r}
datasheet(CMScenario, "STSim_OutputOptions")
sheetData = data.frame(SummaryOutputSC=T, SummaryOutputSCTimesteps=1,
                       SummaryOutputTR=T, SummaryOutputTRTimesteps=1,
                       RasterOutputSC=T, RasterOutputSCTimesteps=1,
                       RasterOutputTR=T, RasterOutputTRTimesteps=1,
                       RasterOutputAge=T, RasterOutputAgeTimesteps=1)
saveDatasheet(CMScenario, sheetData, "STSim_OutputOptions")

# Show the transitions for the scenarios
datasheet(myProject,scenario = c("Current Management Scenario", "No Management Scenario"), name= "STSim_TransitionTarget")
```

## Run Scenarios/Get Results
Run the STSM Model 
```{r}
# Run both scenarios - each Monte Carlo iteration is run in parallel as a separate multiprocessing job
(resultSummary = run(myProject, scenario=c("No Management Scenario", "Current Management Scenario"), jobs=7, summary=T))
```

#Organize the results
## Get the results and organize from STSM
```{r}
# Get the scenario IDs of the 2 result scenarios
resultIDNManagement = subset(resultSummary, parentID==scenarioId(NMScenario))$scenarioId
resultIDCurrentConditions = subset(resultSummary, parentID==scenarioId(CMScenario))$scenarioId

# Get a dataframe of the state class tabular output (for both scenarios combined)
outputStratumState = datasheet(myProject, scenario=c(resultIDNManagement,resultIDCurrentConditions), name="STSim_OutputStratumState")

#Make a matrix of the Results Scenario ID, indexes (CM = 1, NM = 2)
ResultsScenarioID = unique(as.matrix(outputStratumState["ScenarioID"]))

#Assign the results of each scenario to a seperate matrix
CMResults = outputStratumState[ which(outputStratumState$ScenarioID == ResultsScenarioID[1]), ]
NMResults = outputStratumState[ which(outputStratumState$ScenarioID == ResultsScenarioID[2]), ]
```

## Reshape the outputs so that matrix transformations can be preformed to STSM simulation outputs 
No management
```{r}
NMResultsForAnalysis  = NMResults %>% pivot_wider(names_from = "StateClassID", values_from = "Amount") #restructure to create columns to save ha for each iteration/timestep by state

NMResultsForAnalysis = as.matrix(NMResultsForAnalysis[c(6,7,15:19)]) # save columns for the iteration,timestep, state

NMResultsForAnalysis[,3:7][is.na(NMResultsForAnalysis[,3:7])] = 0 #coerce NAs to numeric (0) for analysis

NMResultsForAnalysisCollapse <- matrix(nrow = max(NMResultsForAnalysis[,1])*max(NMResultsForAnalysis[,2]), ncol = 7) #Make a matrix to store collapsed results

counter = 1 #create counter to index through collapsed matrix

for (i in 1:max(NMResultsForAnalysis[,1])) { #index over each iteration
  iteration = NMResultsForAnalysis[ which(NMResultsForAnalysis[,1]== i), ]
  
 for (j in 1:max(iteration[,2])) { #index over each timestep
    timestep = colSums(iteration[ which(iteration[,2]== j), ][,3:7])
    
    NMResultsForAnalysisCollapse[counter,] = array(c(i,j,timestep)) #assign results to the matrix created above
    
    counter = counter + 1
  }   
}

colnames(NMResultsForAnalysisCollapse) <- c("Iteration", "Timestep","Late1:CLS","Early1:ALL","Mid1:OPN","Late1:OPN","Mid1:CLS") #rename the columns in the collapsed 
```
Current Management
```{r}
CMResultsForAnalysis  = CMResults %>% pivot_wider(names_from = "StateClassID", values_from = "Amount") #restructure to create columns to save ha for each iteration/timestep by state

CMResultsForAnalysis = as.matrix(CMResultsForAnalysis[c(6,7,15:19)]) # save columns for the iteration,timestep, state

CMResultsForAnalysis[,3:7][is.na(CMResultsForAnalysis[,3:7])] = 0 #coerce NAs to numeric (0) for analysis

CMResultsForAnalysisCollapse <- matrix(nrow = max(CMResultsForAnalysis[,1])*max(CMResultsForAnalysis[,2]), ncol = 7) #Make a matrix to store collapsed results

counter = 1 #create counter to index through collapsed matrix

for (i in 1:max(CMResultsForAnalysis[,1])) { #index over each iteration
  iteration = CMResultsForAnalysis[ which(CMResultsForAnalysis[,1]== i), ]
  
 for (j in 1:max(iteration[,2])) { #index over each timestep
    timestep = colSums(iteration[ which(iteration[,2]== j), ][,3:7])
    
    CMResultsForAnalysisCollapse[counter,] = array(c(i,j,timestep)) #assign results to the matrix created above
    
    counter = counter + 1
  }   
}

colnames(CMResultsForAnalysisCollapse) <- c("Iteration", "Timestep","Late1:CLS","Early1:ALL","Mid1:OPN","Late1:OPN","Mid1:CLS") #rename the columns in the collapsed 
```

#Quantify Ecosystem Services
## Quantify the habitat area utilized for the endangered species of interest
```{r}
# Import Area Occupied by Federally Listed, Threatened and Endangered Species on each Longleaf Pine state at Eglin AFB in hectares (above) 
EglinAFBSpeciesByAreaOccupied <- read_csv("C:/Users/13022/Box/SERDP ES Project/Model and Results (2019)/Eglin Case Study/SyncroSim Model/ES Quantification Valuation/Area Occupied by Federally Listed Threatened and Endangered Species by Longleaf Pine state at Eglin AFB in hectares.csv")

EglinAFBSpeciesByAreaOccupied$TotalHabitat = rowSums(EglinAFBSpeciesByAreaOccupied[,4:9]) #calculate the total habitat

EglinAFBSpeciesByAreaOccupied$PercentModeledHabitat = EglinAFBSpeciesByAreaOccupied$TotalHabitat / EglinAFBSpeciesByAreaOccupied$TotalHabitat[length(EglinAFBSpeciesByAreaOccupied$TotalHabitat)] #calculate the total percent of the habitat occupied 

#create matrix of multipliers (the percentage of area occupied percentages (below)
EglinAFBSpeciesMultipliersByState = matrix( nrow = 13,ncol = 5)

#area (ha) of habitat occupied in each state class by endangered species / by the total area in the state class
for (i in 1:13){
  EglinAFBSpeciesMultipliersByState[i,] = as.matrix(EglinAFBSpeciesByAreaOccupied[i,4:8] / EglinAFBSpeciesByAreaOccupied[14,4:8]) 
}

rownames(EglinAFBSpeciesMultipliersByState) <- c(EglinAFBSpeciesByAreaOccupied$`Common Name`[1:13]) #change the row names
```
## For each scenario calculate the total area occupied by each species over each iteration/timestep
No Management
```{r}
NMSpeciesQuantifiedFromSTSM <- array(dim = c(max(NMResultsForAnalysis[,1])*max(NMResultsForAnalysis[,2]), 8, 13)) #number of rows (iterations*timesteps), number of columns(iterations, timesteps, classes), number of arrays(species)

for (i in 1:13) {
  for (counter in 1:(max(NMResultsForAnalysis[,1])*max(NMResultsForAnalysis[,2]))){
    NMSpeciesQuantifiedFromSTSM[counter,1:2,i] = array(NMResultsForAnalysisCollapse[counter,1:2]) #Iteration and time step
  
    NMSpeciesQuantifiedFromSTSM[counter,3:7,i] = array(EglinAFBSpeciesMultipliersByState[i,]*
                                                    NMResultsForAnalysisCollapse[counter,3:7])
                                                    #the multiplier*the distribution of vegetation across the states 
    NMSpeciesQuantifiedFromSTSM[counter,8,i] = sum(NMSpeciesQuantifiedFromSTSM[counter,3:7,i])}#total quantities
}

colnames(NMSpeciesQuantifiedFromSTSM) <- c("Iteration", "Timestep","Late1:CLS","Early1:ALL","Mid1:OPN",
                                         "Late1:OPN","Mid1:CLS","Total Quantities") #rename the columns in the collapsed
```
Current Management
```{r}
CMSpeciesQuantifiedFromSTSM <- array( dim = c(max(CMResultsForAnalysis[,1])*max(CMResultsForAnalysis[,2]), 8, 13)) #number of rows, number of columns, number of arrays

for (i in 1:13) {
  for (counter in 1:(max(CMResultsForAnalysis[,1])*max(CMResultsForAnalysis[,2]))){
      CMSpeciesQuantifiedFromSTSM[counter,1:2,i] = array(CMResultsForAnalysisCollapse[counter,1:2]) #Iteration and time step
  
      CMSpeciesQuantifiedFromSTSM[counter,3:7,i] = array(EglinAFBSpeciesMultipliersByState[i,]*
                                                    CMResultsForAnalysisCollapse[counter,3:7])
                                                    #the multiplier*the distribution of vegetation across the states 
  
      CMSpeciesQuantifiedFromSTSM[counter,8,i] = sum(CMSpeciesQuantifiedFromSTSM[counter,3:7,i])} #total quantities
}

colnames(CMSpeciesQuantifiedFromSTSM) <- c("Iteration", "Timestep","Late1:CLS","Early1:ALL","Mid1:OPN",
                                         "Late1:OPN","Mid1:CLS","Total Quantities") #rename the columns in the collapsed
```

#Load CPI Data
Define API Key
```{r}
api_key = '4e05ec7f7f8e475c81b11237551f1f76'
```

Function to process results
```{r}
apiDF <- function(data){
  df <- data.frame(year = as.numeric(data[1][[1]]$year),
                  period = data[1][[1]]$period,
                  periodName = data[1][[1]]$periodName,
      value = as.numeric(data[1][[1]]$value),
                   stringsAsFactors=FALSE)
}
```

CPI Datasets
```{r}
#CPI Years 1990 to 2000
payload1 <- list('seriesid'=c("CUSR0000SA0"),
                'startyear'=1990,
                'endyear'=2000,
                'registrationKey'= api_key)
response1 <- blsAPI(payload1, 2)
json1 <- fromJSON(response1)
CPI_df1 = apiDF(json1$Results$series$data)

#CPI Years 2001 to 2011
payload2 <- list('seriesid'=c("CUSR0000SA0"),
                'startyear'=2001,
                'endyear'=2011,
                'registrationKey'= api_key)
response2 <- blsAPI(payload2, 2)
json2 <- fromJSON(response2)
CPI_df2 = apiDF(json2$Results$series$data)

#CPI Years 2012 to 2020
payload3 <- list('seriesid'=c("CUSR0000SA0"),
                'startyear'=2012,
                'endyear'=2020,
                'registrationKey'= api_key)
response3 <- blsAPI(payload3, 2)
json3 <- fromJSON(response3)
CPI_df3 = apiDF(json3$Results$series$data)
```

Merge into one dataframe
```{r}
CPI_df = bind_rows(CPI_df1, CPI_df2, CPI_df3)
```

Define new CPI (June 2020)
```{r}
new_cpi <- subset(CPI_df, year == 2020 & periodName == "June", select = "value")
```

# Red-cockaded woodpecker (endangered species)
(WTP for guaranteed population survival in South Carolina (1999) / RCW population in South Carolina in 2000) * all households in Florida

Calculate RCW CPI ratio
```{r}
# Get the CPI for the date the RCW paper was published (October 1998)
RCW_Reaves_cpi <- subset(CPI_df, year == 1998 & periodName == "October", select = "value")
# Calculate the ratio for each RCW value
RCW_ratio = (new_cpi / RCW_Reaves_cpi)
```

Values for RCW (derived from Reaves); accepted on 14 October 1998
```{r}
#Open Ended Survey Value
open_ended_mean = 10.28 # (OE)
open_ended_mean_adjusted = RCW_ratio * open_ended_mean # (OE)

open_ended_se = 2.35 # (OE)
open_ended_n = 154 # (OE)
#Payment Card Survey Value
payment_card_mean = 8.35 #(PC) 
payment_card_mean_adjusted = RCW_ratio * payment_card_mean #(PC) 

payment_card_se = 2.14 #(PC) 
payment_card_n = 184 #(PC) 
#Dichotomous Choice Survey Value
dichotomous_choice_mean =13.25 #(DC)
dichotomous_choice_mean_adjusted = RCW_ratio * dichotomous_choice_mean #(DC)

dichotomous_choice_se = 2.20 #(DC)
dichotomous_choice_n = 128 #(DC)

# RCW population in South Carolina in 2000
RCW_pop_in_SC_2020 = 669

# number of households in Florida; estimate from census for 2014-2018
# https://www.census.gov/quickfacts/fact/table/FL,US/PST045219
florida_households = 7621760
```

Calculate $/individual species member
```{r}
dollar_per_RCW_individual_species_member_OE = (open_ended_mean_adjusted/RCW_pop_in_SC_2020)*florida_households 
dollar_per_RCW_individual_species_member_PC =
(payment_card_mean_adjusted/RCW_pop_in_SC_2020)*florida_households 
dollar_per_RCW_individual_species_member_DC =
(dichotomous_choice_mean_adjusted/RCW_pop_in_SC_2020)*florida_households 
```


#Hunting and Fishing
## Eglin Hunting and Fishing Permit Data
Figure 3-20. FY00-10 Total Permits Issued; INRMP 
Extrapolated from figure. Values not actually used in analysis
```{r}
hunting_permits = 8700  

fishing_permits = 6600  
```

## Aiken's Hunting and Fishing WTF estimates 
### Calculate Hunting and Fishing CPI ratio
```{r}
# Get the CPI for 2016, the paper itself was not accessible, but the values in the Benefit Transfer Toolkit have been adjusted to 2016 values
Hunting_Fishing_Aikens_cpi <- subset(CPI_df, year == 2016 & periodName == "January", select = "value")
# Calculate the ratio for each RCW value
Hunting_Fishing_ratio = (new_cpi / Hunting_Fishing_Aikens_cpi)
```

### Hunting
Aiken, R. 2009. Net economic values of wildlife-related recreation in 2006: Addendum to the 2006 National Survey of Fishing, Hunting, and Wildlife-Associated Recreation. Report 2006-5. Washington, DC: U.S. Fish and Wildlife Service.
https://sciencebase.usgs.gov/benefit-transfer/activity/display/7986
```{r}
FL_deer_contingent_valuation = 169.08 #Net economic values of wildlife-related recreation; dollar Per Person Per Day; 2016 adjusted from Benefit Transfer Toolkit

FL_deer_contingent_valuation_adjusted = Hunting_Fishing_ratio * FL_deer_contingent_valuation 
```
Values are coming from tables in: 
2011 National Survey of Fishing, Hunting, and Wildlife-Associated Recreation; U.S. Fish & Wildlife Service 2011; Integrated Natural Resources Management Plan Activities Final Environmental Assessment
```{r}
days_hunting = 5252 # (Thousands) of days hunting in Florida [Table 54]
total_hunters = 242 # (Thousands) of total hunters in Florida [Table 51] 
days_hunting_per_perdays_hunting_per_person
son = days_hunting / total_hunters #days per person
```
Hunting Value
```{r}
FL_deer_hunting_value = FL_deer_contingent_valuation_adjusted * days_hunting_per_person
```

### Fishing
Aiken, R. 2009. Net economic values of wildlife-related recreation in 2006: Addendum to the 2006 National Survey of Fishing, Hunting, and Wildlife-Associated Recreation. Report 2006-5. Washington, DC: U.S. Fish and Wildlife Service.
https://sciencebase.usgs.gov/benefit-transfer/activity/display/9008
```{r}
FL_bass_contingent_valuation = 70.26 #Net economic values of wildlife-related recreation; dollar Per Person Per Day; 2016 adjusted from Benefit Transfer Toolkit

FL_bass_contingent_valuation_adjusted  = Hunting_Fishing_ratio * FL_bass_contingent_valuation
```
Values are coming from tables in: 
2011 National Survey of Fishing, Hunting, and Wildlife-Associated Recreation; U.S. Fish & Wildlife Service 2011; Integrated Natural Resources Management Plan Activities Final Environmental Assessment
```{r}
days_fishing = 57594  # (Thousands) of days fishing in Florida [Table 59]
total_anglers = 3092 # (Thousands) of total anglers in Florida [Table 51] 
days_fishing_per_person = days_fishing / total_anglers #days per person
days_fishing_per_person
```
Fishing Value
```{r}
FL_bass_fishing_value = FL_bass_contingent_valuation_adjusted * days_fishing_per_person
```






#Carbon
Becker, 2011
* Used values for longleaf pine flatwoods
* The original analysis only considered unburned restored longleaf pine flatwoods

Above Ground Carbon (page 35)
```{r}
aboveground__biomass_carbon_stock <- data.frame(
   mean = c(2.27, 0.95), # kg C/m^2; mean
   se = c(0.63, 0.21), # kg C/m^2; standard error
   n = c(5, 5)) # number of samples
row.names(aboveground__biomass_carbon_stock) = c("unburned_restored","burned_restored")

aboveground__biomass_carbon_stock$sd = aboveground__biomass_carbon_stock$se * sqrt(aboveground__biomass_carbon_stock$n)

aboveground__biomass_carbon_stock
```
Soil Carbon (page 37)
```{r}
soil_carbon_stock <- data.frame(
   mean = c(14.9, 17.1), # kg C/m^2; mean
   se = c(3.5, 2.6), # kg C/m^2; standard error
   n = c(5, 5)) # number of samples

row.names(soil_carbon_stock) = c("unburned_restored","burned_restored")

soil_carbon_stock$sd = soil_carbon_stock$se * sqrt(soil_carbon_stock$n)

soil_carbon_stock
```

Charcoal (page 26 - 27)
```{r}
charcoal <- data.frame(
   mean = c(29.0 * 0.001, 67.4 * 0.001), # kg C/m^2; mean
   se = c(16.7 * 0.001, 26.0 * 0.001), # kg C/m^2; standard error
   n = c(5, 5)) # number of samples

row.names(charcoal) = c("unburned_restored","burned_restored")

charcoal$sd = charcoal$se * sqrt(charcoal$n)

charcoal
```

Litter Carbon Stock (page 24)
```{r}
litter_carbon_stock <- data.frame(
   mean = c(0.20, 0.10), # kg C/m^2; mean
   se = c(0.06, 0.03), # kg C/m^2; standard error
   n = c(5, 5)) # number of samples

row.names(litter_carbon_stock) = c("unburned_restored","burned_restored")

litter_carbon_stock$sd = litter_carbon_stock$se * sqrt(litter_carbon_stock$n)

litter_carbon_stock
```
Draw samples from log normal distribution to ensure that all of the samples are positive 
```{r}
unburned_aboveground_biomass_carbon_stock_samples = c(rlnorm(10000, 
                                                          mean = aboveground__biomass_carbon_stock[1,1], 
                                                          sd = aboveground__biomass_carbon_stock[1,4]))

unburned_soil_carbon_stock_samples = c(rlnorm(10000, 
                                  mean = soil_carbon_stock[1,1], 
                                  sd = soil_carbon_stock[1,4]))

unburned_charcoal_samples = c(rlnorm(10000, 
                                  mean = charcoal[1,1], 
                                  sd = charcoal[1,4]))

unburned_litter_carbon_stock_samples = c(rlnorm(10000, 
                                  mean = litter_carbon_stock[1,1], 
                                  sd = litter_carbon_stock[1,4])) 
```

```{r}
total_carbon_samples = (unburned_aboveground_biomass_carbon_stock_samples +  
                       unburned_litter_carbon_stock_samples +
                       unburned_charcoal_samples +
                       unburned_litter_carbon_stock_samples) * 10  # 10* kg/m2 -> mg/ha
print("median:")
median(total_carbon_samples) 
print("mean:")
mean(total_carbon_samples)
```

```{r}
#plot(density(total_carbon_samples),xlim=range(0,1000)) # plots the results
#lines(density(unburned_aboveground_biomass_carbon_stock_samples), col="blue")
#lines(density(unburned_litter_carbon_stock_samples), col="red")
#lines(density(unburned_charcoal_samples), col="magenta")
#lines(density(unburned_litter_carbon_stock_samples), col="yellow")
```


```{r}
# number of samples
carbon = matrix( 
   c(2, 2, 2, 2, 2), # the data elements 
   nrow=5,           # number of rows 
   ncol=1,           # number of columns 
   byrow = TRUE) 
```


```{r}
x = NMResultsForAnalysis[,3:7] %*% carbon
mode(NMResultsForAnalysis)
class(carbon)
```

